// This frame is like the main method in the X example above
import javax.swing.border.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class NameFrame extends JFrame
{ 
  public static void main( String[] args )
  {
    NameFrame theWindow = new NameFrame( );
    theWindow.show( );
  }

  private JTextField nameInputField;
  private JLabel firstNameLabel;
  private JLabel lastNameLabel;

  public NameFrame( )
  {
    this.setTitle( "Name Frame" );
    this.setSize( 200, 110 );
    this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

    Container contentPane = this.getContentPane();
    contentPane.setLayout( new GridLayout( 3, 2, 4, 4 ) );

    contentPane.add( new JLabel( "  Enter name "  ) );
    nameInputField = new JTextField( );
    contentPane.add( nameInputField );

    contentPane.add( new JLabel( "  First Name" ) );
    firstNameLabel = new JLabel("??" ) ;
    contentPane.add( firstNameLabel );

    contentPane.add( new JLabel( "  Last Name" ) );
    lastNameLabel = new JLabel( "??") ;
    contentPane.add( lastNameLabel  );

    NameInputListener inputListener = new NameInputListener( );
    nameInputField.addActionListener( inputListener );
  }

  private class NameInputListener implements ActionListener
  {
    public void actionPerformed( ActionEvent e )
    {
      // Send the text to the factory and get a class back
      Namer namer = NameFactory.getNamer( nameInputField.getText( ) );

      // Compute the first and last names using the returned class
      String firstName = namer.getFirst( );
      String lastName = namer.getLast( );

      firstNameLabel.setText( firstName );
      lastNameLabel.setText( lastName );
    }
  }
}
